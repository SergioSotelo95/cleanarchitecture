package com.example.cleanarchitecture.domain

import com.example.cleanarchitecture.data.PictureRepository
import com.example.cleanarchitecture.data.model.PictureModel

class GetSinglePictureUseCase {
    private val repository = PictureRepository()
    suspend operator fun invoke(int: Int): PictureModel = repository.getOnePicture(int)

}