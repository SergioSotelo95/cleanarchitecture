package com.example.cleanarchitecture.domain

import android.app.Application
import com.example.cleanarchitecture.data.PictureRepository
import com.example.cleanarchitecture.data.model.PictureModel
import javax.inject.Inject

class UpdateDatabaseUseCase @Inject constructor() {
    private val repository = PictureRepository()
    suspend operator fun invoke(application: Application, listOfPictures: List<PictureModel>) = repository.updateDatabase(application, listOfPictures)
}