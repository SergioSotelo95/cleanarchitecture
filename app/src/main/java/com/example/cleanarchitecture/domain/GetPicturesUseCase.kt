package com.example.cleanarchitecture.domain

import com.example.cleanarchitecture.data.PictureRepository
import com.example.cleanarchitecture.data.model.PictureModel
import javax.inject.Inject

class GetPicturesUseCase @Inject constructor() {
    private val repository = PictureRepository()
    suspend operator fun invoke(): List<PictureModel> = repository.getAllPictures()
}