package com.example.cleanarchitecture.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.cleanarchitecture.data.model.PictureModel

class PictureDiffCallback: DiffUtil.ItemCallback<PictureModel>() {
    override fun areItemsTheSame(oldItem: PictureModel, newItem: PictureModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PictureModel, newItem: PictureModel): Boolean {
        return oldItem == newItem
    }
}