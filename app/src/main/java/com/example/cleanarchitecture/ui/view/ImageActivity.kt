package com.example.cleanarchitecture.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.cleanarchitecture.R
import com.example.cleanarchitecture.databinding.ActivityImageBinding
import com.example.cleanarchitecture.domain.GetSinglePictureUseCase
import kotlinx.coroutines.launch

class ImageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityImageBinding
    var getSinglePictureUseCase = GetSinglePictureUseCase()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch {
            val result = getSinglePictureUseCase(intent.getIntExtra("id", 2))
            val context = binding.ImageCard.context
            val imageURL = result.url + ".png"

            Glide.with(context).load(imageURL).placeholder(R.drawable.error).into(binding.Image)

        }
    }
}