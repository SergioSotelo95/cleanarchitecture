package com.example.cleanarchitecture.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cleanarchitecture.R
import com.example.cleanarchitecture.data.model.PictureModel
import com.example.cleanarchitecture.databinding.ImageListCardBinding
import com.example.cleanarchitecture.ui.view.ImageActivity

class PictureListAdapter :
    ListAdapter<PictureModel, PictureListAdapter.PictureListHolder>(PictureDiffCallback()) {

    inner class PictureListHolder(val binding: ImageListCardBinding) :
        RecyclerView.ViewHolder(binding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureListHolder {
        return PictureListHolder(
            ImageListCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PictureListHolder, position: Int) {
        holder.binding.apply {
            val picture = getItem(position)
            ImageTitle.text = picture.title
            val thumbnailURL = picture.thumbnailUrl + ".jpg"
            Glide.with(ThumbnailLayout.context).load(thumbnailURL).placeholder(R.drawable.error)
                .into(Thumbnail)
            ThumbnailLayout.setOnClickListener {
                val intentImage = Intent(ThumbnailLayout.context, ImageActivity::class.java)
                intentImage.putExtra("id", picture.id)
                ContextCompat.startActivity(ThumbnailLayout.context, intentImage, null)
            }
        }
    }

}