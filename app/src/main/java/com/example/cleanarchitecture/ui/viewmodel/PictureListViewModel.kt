package com.example.cleanarchitecture.ui.viewmodel


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.cleanarchitecture.data.model.PictureModel
import com.example.cleanarchitecture.domain.GetPicturesUseCase
import com.example.cleanarchitecture.domain.UpdateDatabaseUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PictureListViewModel @Inject constructor(
    application: Application,
    private val getPicturesUseCase: GetPicturesUseCase = GetPicturesUseCase(),
    private val updateDatabaseUseCase: UpdateDatabaseUseCase = UpdateDatabaseUseCase()
) : AndroidViewModel(application) {
    val pictureListModel = MutableLiveData<List<PictureModel>>()
    val isLoading = MutableLiveData<Boolean>()

    init {
        updateResults()
    }

    fun updateResults() {
        viewModelScope.launch {
            isLoading.value = true
            val result = getPicturesUseCase()
            if (!result.isNullOrEmpty()) {
                pictureListModel.postValue(result)
                updateDatabaseUseCase(getApplication(), result)
            }
            isLoading.value = false
        }
    }

}