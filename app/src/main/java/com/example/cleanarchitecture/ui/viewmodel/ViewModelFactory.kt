package com.example.cleanarchitecture.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class ViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PictureListViewModel::class.java)) {
            return PictureListViewModel(application) as T
        }
        throw IllegalArgumentException("Can't create ViewModel")
    }
}
