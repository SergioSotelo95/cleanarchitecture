package com.example.cleanarchitecture.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cleanarchitecture.databinding.ActivityMainBinding
import com.example.cleanarchitecture.ui.adapter.PictureListAdapter
import com.example.cleanarchitecture.ui.viewmodel.PictureListViewModel
import com.example.cleanarchitecture.ui.viewmodel.ViewModelFactory
import dagger.hilt.android.AndroidEntryPoint


const val TAG = "MainActivity"

@AndroidEntryPoint
open class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        val viewModel = ViewModelProvider(
            this,
            ViewModelFactory(this.application)
        ).get(PictureListViewModel::class.java)

        viewModel.pictureListModel.observe(this) {
            (binding.RV.adapter as PictureListAdapter).submitList(it)
        }
        viewModel.isLoading.observe(this) {
            binding.swipeRefreshLayout.isRefreshing = it
        }
        binding.swipeRefreshLayout.setOnRefreshListener { viewModel.updateResults() }

    }

    private fun setupRecyclerView() = binding.RV.apply {
        adapter = PictureListAdapter()
        layoutManager = LinearLayoutManager(this@MainActivity)
    }

}