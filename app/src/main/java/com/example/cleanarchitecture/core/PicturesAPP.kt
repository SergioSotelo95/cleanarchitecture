package com.example.cleanarchitecture.core

import android.app.Application
import androidx.room.Room
import com.example.cleanarchitecture.data.database.PicturesDataBase
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PicturesAPP : Application() {
    val room by lazy {
        Room
            .databaseBuilder(this, PicturesDataBase::class.java, "pictures")
            .build()
    }
}