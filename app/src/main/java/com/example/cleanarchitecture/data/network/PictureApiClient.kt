package com.example.cleanarchitecture.data.network

import com.example.cleanarchitecture.data.model.PictureModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PictureApiClient {
    @GET("/photos")
    suspend fun getAllPictures(): Response<List<PictureModel>>

    @GET("/photos/{id}")
    suspend fun get1Picture(@Path("id") id: Int): Response<PictureModel>

    @GET("/photos")
    suspend fun getCustomPictures(@Query("id") id: MutableList<Int>): Response<List<PictureModel>>

}