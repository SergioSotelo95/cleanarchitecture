package com.example.cleanarchitecture.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.cleanarchitecture.data.model.PictureModel

@Dao
interface PicturesDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(picturesList: List<PictureModel>)

    @Query("SELECT * FROM PictureModel")
    fun get(): LiveData<List<PictureModel>>
}