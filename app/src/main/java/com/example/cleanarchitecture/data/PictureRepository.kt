package com.example.cleanarchitecture.data

import android.app.Application
import android.util.Log
import com.example.cleanarchitecture.core.PicturesAPP
import com.example.cleanarchitecture.data.model.PictureModel
import com.example.cleanarchitecture.data.network.PictureListService
import com.example.cleanarchitecture.data.network.SinglePictureService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class PictureRepository() {
    private val api = PictureListService()
    private val singlePictureApi = SinglePictureService()

    suspend fun getAllPictures(): List<PictureModel> {
        return api.getPictures()
    }

    suspend fun getOnePicture(int: Int): PictureModel {
        return singlePictureApi.getSinglePicture(int)
    }

    suspend fun updateDatabase(application: Application, listOfPictures: List<PictureModel>) {
        withContext(Dispatchers.IO) {
            try {
                (application as PicturesAPP).room.picturesDao().insert(listOfPictures)

            } catch (e: Exception) {
                Log.e("Repository", "Error ${e.message}")
            }
        }
    }

}