package com.example.cleanarchitecture.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PictureModel(
    val albumId: Int,
    @PrimaryKey
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)