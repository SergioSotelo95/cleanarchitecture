package com.example.cleanarchitecture.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.cleanarchitecture.data.model.PictureModel

@Database(
    entities = [PictureModel::class],
    version = 1,
    exportSchema = false
)
abstract class PicturesDataBase : RoomDatabase() {
    abstract fun picturesDao(): PicturesDAO
}