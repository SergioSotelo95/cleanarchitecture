package com.example.cleanarchitecture.data.network

import com.example.cleanarchitecture.core.RetrofitHelper
import com.example.cleanarchitecture.data.database.PicturesDataBase
import com.example.cleanarchitecture.data.model.PictureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PictureListService() {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getPictures(): List<PictureModel> {

        return withContext(Dispatchers.IO) {
            val response = retrofit.create(PictureApiClient::class.java)
                .getCustomPictures((1..25).toMutableList())
            response.body() ?: emptyList()
        }
    }
}