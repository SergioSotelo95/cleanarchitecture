package com.example.cleanarchitecture.data.network

import com.example.cleanarchitecture.core.RetrofitHelper
import com.example.cleanarchitecture.data.model.PictureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SinglePictureService() {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getSinglePicture(id: Int): PictureModel {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(PictureApiClient::class.java).get1Picture(id)
            response.body()!!
        }
    }
}
